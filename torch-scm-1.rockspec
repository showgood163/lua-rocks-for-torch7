package = "torch"
version = "scm-1"

source = {
   url = "git://github.com/torch/torch7.git",
}

description = {
   summary = "Torch7",
   detailed = [[
   ]],
   homepage = "https://github.com/torch/torch7",
   license = "BSD"
}

dependencies = {
   "lua >= 5.1",
   "paths >= 1.0",
   "cwrap >= 1.0"
}

build = {
   type = "command",
   build_command = [[
   cmake -E make_directory build && cd build && cmake .. \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DLUADIR=/usr/share/lua/5.1 \
		-DLIBDIR=/usr/lib/lua/5.1 \
		-DLUA_BINDIR=/usr/bin \
		-DLUA_INCDIR=/usr/include/luajit-2.1 \
		-DLUA_LIBDIR=/usr/lib \
		-DLUALIB=/usr/lib/libluajit-5.1.so \
		-DLUA=/usr/bin/luajit && $(MAKE)
]],
   install_command = "cd build && $(MAKE) install"
}
